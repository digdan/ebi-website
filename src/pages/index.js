import * as React from "react"

import Layout from "../components/layout"
import Seo from "../components/seo"
import About from "../components/about";
import Reviews from "../components/reviews";

const IndexPage = () => (
  <Layout>
    <Seo title="Pathway Ebooks" />
    <About/>
    <Reviews/>
  </Layout>
)

export default IndexPage
