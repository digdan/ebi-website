import * as React from "react"
import PropTypes from "prop-types"
import { Link } from "gatsby"
import { StaticImage } from "gatsby-plugin-image"


const Header = ({ siteTitle }) => (
  <div class="header-container section" id="home">
    <header class="wrapper clearfix">
        <h1 class="logo">
          <a href="#home">
            <StaticImage
              src="../images/logo_dark.png"
              quality={95}
              alt="Logo"
            />
          </a>
        </h1>
        <nav>
            <ul class="nav">
                <li><a href="#about">About</a></li>
                <li><a href="#review">Reviews</a></li>
                <li class="last"><a href="#Your Link Here">Download</a></li>
            </ul>
        </nav>
    </header>
    <div class="wrapper clearfix">
      <div class="cta">
        <h2>Selling eBooks</h2>
        <p><strong>Curabitur blandit tempus porttit orman. Maecenas sed diam eget risuarius blandit.</strong></p>
        <p>Etiam porta sem malesuada magna mollis euismod. Maecenas sed diam eget risus varius blandit sit amet non magna. Nullam quis risus eget urna mollis ornare vel eu leo Praesent commodo cursus magna.</p>
        <p><a href="#Your Link Here" class="btn blue">Download eBook <span>$8</span></a></p>
        <p class="softLink"><a href="#Your Link Here">Try our Free Sample Book First </a></p>
        <p class="softtext">Email me when it’s ready</p>
        <input type="text" class="field" name="email" value="" placeholder="Your Email" />
        <input type="button" class="btn blue" name="" value="Add Me" />      
      </div>
      
      <div class="book1">
        <StaticImage src="../images/books.jpg" width={666} alt="Insert title of Book" />
      </div>
      <div class="book2">
        <StaticImage src="../images/book_single.jpg" width={666} alt="Insert title of Book" />
      </div>
    </div>
  </div>
)

Header.propTypes = {
  siteTitle: PropTypes.string,
}

Header.defaultProps = {
  siteTitle: ``,
}

export default Header
