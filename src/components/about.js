import React from "react";
import { StaticImage } from "gatsby-plugin-image";

const About = ({ siteTitle }) => {
    return (
    <div class="main-container section about" id="about">            
        <div class="main wrapper clearfix">
            <div class="callout">
                <p>Using the outcomes from the eBook, you can put together a plan for the most effective marketing strategy to <span>get the best results</span> for your business.</p>
            </div>
            
            <div class="half left">
                <h3>What Booker Teaches</h3>
                <p>Etiam porta sem malesuada magna mollis euismod. Maecenas sed diam eget risus varius blandit sit amet non magna. Donec ullamcorper nulla.</p>
                
                <h4>The six most important tips you’ll learn</h4>
                <ol>
                    <li>Praesent commodo cursus magna, ligula porta felis euismod semper.l nisl consectetur etonec ullamcorper nulla non metus auctor fringilla. Nullam quis risus eget urna mollis ornare velo.</li> 
                
                    <li>Vestibulum id ligula porta felis euismod semper. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mususce dapibus.</li> 
                
                    <li>Tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Cras justo odio, dapibus ac facilisis in, egestas eget quam.</li>
                
                    <li>Magna, ligula porta felis euismod semper.l nisl consectetur etonec ullamcorper nulla non metus auctor fringilla. Nullam quis risus eget urna mollis ornare velomunis.</li> 
                
                    <li>Vestibulum id ligula porta felis euismod semper. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mususce dapibus.</li>
                
                    <li>Praesent commodo cursus magna, ligula porta felis euismod semper.l nisl consectetur etonec ullamcorper nulla non metus auctor fringilla. Nullam quis risus eget urna mollis ornare velo.</li>
                </ol>	
            </div>
            
            
            <div class="half right pages">
                <h3>Sample Pages</h3>
                <ul>
                    <li>
                        <div class="mosaic-block circle">
                            <a class="fancybox mosaic-overlay" href="images/image.jpg" data-fancybox-group="gallery"></a>
                            <div class="mosaic-backdrop"><StaticImage src="../images/thumb.jpg"/></div>
                        </div>
                        <em><span>Page 5.</span> Tellus ac cursus commodo, tortor mauris condimentum nibh.</em>
                    </li>
                    
                    <li class="even">
                        <div class="mosaic-block circle">
                            <a class="fancybox mosaic-overlay" href="images/image.jpg" data-fancybox-group="gallery"></a>
                            <div class="mosaic-backdrop"><StaticImage src="../images/thumb.jpg"/></div>
                        </div>
                           <em><span>Page 12.</span> Fusce dapibus, tellus ac cursus commodo, tortor mauris.</em>
                    </li>
                    
                    <li>
                        <div class="mosaic-block circle">
                            <a class="fancybox mosaic-overlay" href="images/image.jpg" data-fancybox-group="gallery"></a>
                            <div class="mosaic-backdrop"><StaticImage src="../images/thumb.jpg"/></div>
                        </div>
                        <em><span>Page 25.</span> Tellus ac cursus commodo, tortor mauris condimentum nibh.</em>
                    </li>
                    
                    <li class="even">
                        <div class="mosaic-block circle">
                            <a class="fancybox mosaic-overlay" href="images/image.jpg" data-fancybox-group="gallery"></a>
                            <div class="mosaic-backdrop"><StaticImage src="../images/thumb.jpg"/></div>
                        </div>
                        <em><span>Page 41.</span> Tellus ac cursus commodo, tortor mauris condimentum nibh.</em>
                    </li>
                    
                    <li>
                        <div class="mosaic-block circle">
                            <a class="fancybox mosaic-overlay" href="images/image.jpg" data-fancybox-group="gallery"></a>
                            <div class="mosaic-backdrop"><StaticImage src="../images/thumb.jpg"/></div>
                        </div>
                        <em><span>Page 45.</span> Tellus ac cursus commodo, tortor mauris condimentum nibh.</em>
                    </li>
                    
                    <li class="even">
                        <div class="mosaic-block circle">
                            <a class="fancybox mosaic-overlay" href="images/image.jpg" data-fancybox-group="gallery"></a>
                            <div class="mosaic-backdrop"><StaticImage src="../images/thumb.jpg"/></div>
                        </div>
                        <em><span>Page 63.</span> Tellus ac cursus commodo, tortor mauris condimentum nibh.</em>
                    </li>
                </ul>	
            </div>
        </div> 
    </div>
    )
}

export default About;