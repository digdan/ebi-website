import React from "react";
import { StaticImage } from "gatsby-plugin-image";

const Reviews = ({ siteTitle }) => {
    return (
        <div class="main-container section review" id="review">
        	<div class="main wrapper clearfix">
        		<div class="callout">
        			<p>Using the outcomes from the eBook, you can put together a plan for the most effective marketing strategy to <span>get the best results</span> for your business.</p>
        		</div>  		
        		<div class="twoThirds right">
        			<h3>eBook Reviews</h3>
        			<p class="quote">Etiam porta sem malesuada magna mollis euismod. Maecenas sed diam eget risus varius blandit sit amet non magna. Donec ullamcorper nulla non metus auctor fringilla.</p> 
        			<h6>Max Pleasant, Daily Tribune</h6>
        			
        			<hr/>
        			<p class="quote">Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Sed posuere consectetur est at lobortis.Etiam porta sem malesuada magna mollis euismod. Maecenas sed diam eget risus varius blandit sit amet non magna. Donec ullamcorper nulla non metus auctor fringilla. </p>
        			<h6>Janice Wince, Amazon</h6>
        			
        			<hr/>
        			<p class="quote">Etiam porta sem malesuada magna mollis euismod. Maecenas sed diam eget risus. Duis mollis est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit.Etiam porta sem malesuada magna mollis euismod.</p>
        			<h6>Joy Luck, Telegraph</h6>
        			
        			<hr/>
        			<p class="quote">Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Sed posuere consectetur est at lobortis.Etiam porta sem malesuada magna mollis euismod. Maecenas sed diam eget risus varius blandit sit amet non magna. Donec ullamcorper nulla non metus auctor fringilla.</p> 
        			<h6>Phil Groud, Penguin</h6>
        			
        			<hr/>
        			<p class="quote">Etiam porta sem malesuada magna mollis euismod. Maecenas sed diam eget risus. Duis mollis est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit.Etiam porta sem malesuada magna mollis euismod.</p>
        			<h6>Joy Luck, Telegraph</h6>
        		</div>
        		<div class="third left author">
        			<h3>About The Author</h3>
        			<h5>Mr Johnathan Authorberry</h5>
        			<h6>Published Author</h6>
        			<p>Etiam porta sem malesuada magna mollis euismod. Maecenas sed diam eget risus varius blandit sit amet non magna.</p> 
        			<p>Nullam quis risus eget urna mollis ornare vel eu leo Praesent commodo cursus magna, ligula porta felis euismod semper.l nisl consectetur etonec ullamcorper nulla non metus auctor fringilla. Nullam quis risus eget urna mollis ornare vel eu leo. Vestibulum id ligula porta felis euismod semper. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Maecenas faucibus mollis interdum.</p>
        			<p>Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mususce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.</p>
        			<p><a href="#Your Link Here" class="btn blue">Download eBook <span>$8</span></a></p>
        		</div>        		
        	</div> 
        </div> 
    )
}

export default Reviews;